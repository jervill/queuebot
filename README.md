
## Running the server

To run this example yourself, add these files to a new directory and run the following commands there:

```
$ npm install
$ npm run web
```

If the server started as expected, you'll see something like the following emitted:

```
info: Atlassian Connect add-on started at http://hostname.local:3000
```

To double check that the server is running correctly, try requesting it's add-on descriptor:

```
$ curl http://hostname.local:3000/addon/capabilities
```

A successful request will return a HipChat capabilities descriptor for the add-on.

## Optimizing your dev loop

To get the server to restart automatically when code changes, [nodemon](https://github.com/remy/nodemon) is recommended, and can be installed by simply running the following:

```
$ npm install -g nodemon
```

Then you can run this command to start the server for development:

```
$ npm run web-dev
```

In the example above, `nodemon` is set up to monitor js, json, css, and hbs (handlebars) files.  If your add-on use additional file types, you may want to add them to the `web-dev` script configuration in `package.json`.

If you encounter errors while following these steps, double check that you're using Node v0.11.x and that all of the above dependencies installed correctly.


## Setting up keen.io to capture analytics from your dev environment

Analytics will work if you have a KEEN_PROJECT_ID and a KEEN_WRITE_KEY set on your environment. I have Keen.io running in Heroku so I did this:

```
heroku config -s | grep KEEN >> .env
source .env
```


## Preparing the add-on for installation

Now that you have a server running, you'll want to try it somehow. The easiest way to test with hipchat.com while developing on your local machine is to use [ngrok](https://ngrok.com).  Download and install it now if you need to -- it's an amazing tool that will change the way you develop and share web applications.

Now you can start a secure tunnel (in a new console) to your add-on that's accessible by the outside world, which will allow you to install it at hipchat.com while you test it:

```
$ npm run tunnel
```

That command will start ngrok and output the address of your new tunnel, which should look something like `https://3a4bfceb.ngrok.com`.  This will be the value you use for your "local base url" needed by the Installation step.

While ngrok will forward both HTTP and HTTPS, for the protection of you and your HipChat group members, you should always use HTTPS when running your add-on on the public internet.

For installing your add-on into a HipChat server instance, please see: [[Developing with a private HipChat server instance]]

## Installation

### Configuring the add-on's local base url

Now, we need to tell the add-on server where it's running so that it can successfully be installed.  You can do this in one of the following ways, using the local base url determined in the prior step, appropriate to your environment:

1. Set the `LOCAL_BASE_URL` environment variable when you start the server:

```
$ LOCAL_BASE_URL=https://xxxxxxxx.ngrok.com npm run web-dev
```

2. Add it to your add-on's `package.json` configuration in the `development` section:

```
"development": {
  "localBaseUrl" : "https://xxxxxxxx.ngrok.com",
  "port": 3000
},
```

The `ac-koa-hipchat` library first looks for the configuration values it needs as environment variables, and then in the current runtime environment section of the configuration.  If the local base url isn't not found in either location, it defaults to `http://<hostname>:$PORT`, but when advertising that address it can't be properly installed with hipchat.com.

Which section of the configuration is used (e.g. `production`, `development`, `test`, etc) depends on the value of the environment variable `NODE_ENV`, whose value defaults to `development`.

When properly configured, you'll see the server report the new local base url when it starts up:

```
info: Atlassian Connect add-on started at https://xxxxxxxx.ngrok.com
```

__Note__: by signing up for an ngrok account, you can specify a generally stable, custom subdomain for even easier iterative development.  See [ngrok](http://ngrok.com) for more information.

### Manually installing the add-on using HipChat's admin UI

To install your add-on into HipChat, you have to register your add-on's capabilities descriptor.

HipChat add-ons can operate inside a room or within the entire account.  When developing, you should probably register your add-on inside a room you've created just for testing. Also, you can only register add-ons inside a room where you are an administrator.

To register your add-on descriptor, navigate to the rooms administration page at `https://<your-account>.hipchat.com/rooms` (or whatever url your private server is running at, if appropriate).  Then select one of your rooms in the list.  In the following page, select `Integrations` in the sidebar, and then click the "Build and install your own integration" link at the bottom of the page:

![Installation Screenshot](https://s3.amazonaws.com/uploads.hipchat.com/10804/124261/ujDtrkh5UBsKs2Y/upload.png)

Paste your descriptor url in the `Integration URL` field of the opened dialog and then click `Add integration`.  This will initiate the installation of your add-on for that room.

# Example

The example above comes from the following example project:

* [Greeter](https://bitbucket.org/atlassianlabs/ac-koa-hipchat-greeter)
