var keenIO = require('keen.io');
var keen = {};

var keenVariablesExist = process.env['KEEN_PROJECT_ID'] && process.env['KEEN_READ_KEY'] && process.env['KEEN_WRITE_KEY'];
var analyticsIsEnabled = process.env['NODE_ENV'] == 'production' || process.env['DEV_ANALYTICS'] == 'true';

if (keenVariablesExist && analyticsIsEnabled) {
    keen = keenIO.configure({
        projectId: process.env['KEEN_PROJECT_ID'],
        readKey: process.env['KEEN_READ_KEY'],
        writeKey: process.env['KEEN_WRITE_KEY']
    });
} else {
    console.log("No analytics for you");
    keen.addEvent = function() {}; // dummy function so that Queuebot doesn't choke on 'addEvent' while keen is disabled
    keen.request = function() {}; //  ^

    // can add other fake functions as needed here
}

module.exports = keen;