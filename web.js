// Require the 'ac-koa' module, and then tell it to load the 'hipchat' adapter
// from 'ac-koa-hipchat'
var ack = require('ac-koa').require('hipchat');
// Require our package.json file, which doubles as the configuration from which
// we'll generate the add-on descriptor and server's runtime parameters
var pkg = require('./package.json');
// Create the base Koa app, via an 'ac-koa' factory method that helps preconfigure
// and decorate the app object
var app = ack(pkg);
var Q = require('./queuebot');

// Now build and mount an AC add-on on the Koa app; we can either pass a full or
// partial descriptor object to the 'addon()' method, or when we provide none, as
// in this example, we can instead create the descriptor using a product-specific
// builder API
var addon = app.addon()
    // Use the hipchat descriptor builder
    .hipchat()
    // Indicate that the descriptor should mark this as installable in rooms
    .allowRoom(true)
    // Provide the list of permissions scopes the add-on requires
    .scopes('send_notification');

addon.onWebhook('install', function *() {
    // use the koa context (this) to access request, response, tenant info, and tenant services normally
    yield this.roomClient.sendNotification('QueueBot has been installed to this room!');
});

addon.webhook('room_message', /.*/, function *() {
    //console.log(this); //dump `this` to console. useful only when you forget the structure of `this`
    //console.log(this.sender.name + ': ' + this.content);
    //yield this.roomClient.sendNotification('ok');
    Q(this).handle();
});


// Now that the descriptor has been defined along with a useful webhook handler,
// start the server
app.listen();