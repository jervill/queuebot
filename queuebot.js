//q.js
var _ = require('lodash');
var REDIS_KEY = 'queuebot';
var moment = require('moment');
var uuid = require('node-uuid');
var keen = require('./keen-helper');

var QueueBot = function( addon ) {
	var bot = {
		// PLAYED BY BRENT SPINER
		data: {
			list: [],
			demoIgnore: [],
			kickoffIgnore: [],
            discoveryIgnore: {
                demo: [],
                kickoff: []
            },
            lastAlertTime: null, //I believe this entire 'data' object is purely for data model reference at this point
			whoToBug: "@here"
		},

		// THE THINGS THAT GET SERIAL KILLERS CAUGHT
        patterns: {
            //qbot [foo bar baz whatever] should call someFunction is
            //someFunction : /^\/qbot\sfoo\s(.*)$/  < a regular expression that should trigger the 'someFunction' function
            showHelp: /^\/qbot\sh[a|e]lp/i, // i.e. /qbot halp will call a function called 'showHelp'
            showList: /^\/qbot\slist$/i,
            showIgnore: /^\/qbot\signore$/i,
            likeTurtles: /turtle/i,
            calmDown: /\(tableflip\)/i,
			bug: /^\/qbot\sbug\s(.*)/i,
            add: /^\/qbot\sadd\s(.*)/i,
            addKickoff: /^\/qbot\skickoff\s(.*)/i,
            addDemo: /^\/qbot\sdemo\s(.*)/i,
            destroyList: /^\/qbot\sdestroy\slist$/i,
            destroyIgnore: /^\/qbot\sdestroy\signore$/i,
            dismissItem: /^\/qbot\s(dismiss|remove)/i,
            test: /^\/qbot\stest/i,
            createKeyword: /^\/qbot\screate\skeyword\s\w+/i,
            listKeywords: /^\/qbot\skeywords$/i,
            hardReset: /^\/qbot\shard\sreset\sseriously/i, // "/qbot hard reset seriously" to wipe all existing data for a room
            keenShowAverageTime: /^\/qbot\sstats/i
        },

		// THE THING YOU PICK UP THE BOT WITH
		handle: function () {
			//console.log('you asked me to interpret the message \'' + addon.content + '\'...');

			var didNotCallFunction = true;
			var startedWithSlashCommand = /^\/qbot/.test(addon.content);

			_.each(bot.patterns, function (regex, funct) {
				//this code is called on each item in the list of patterns
				if (regex.test(addon.content)) {
					//console.log("YOU ARE TRYING TO CALL THE " + funct + " FUNCTION???");
					bot[funct]();
					didNotCallFunction = false;
				}
			});

			if (didNotCallFunction) {
                if (startedWithSlashCommand) {
                    console.log("I have no idea what you're trying to do so I'm going to pretend you need halp.")
                    bot.showHelp();
                } else {
                    /// FIXME (every uncaught room message (by design) triggers these lower redis fetches.. we'll see how that goes)
                    bot.fetchDiscovery().then(function () {
                        _.each(bot.data.discoveryIgnore, function (values, keyword) {
                            console.log(keyword, values)
                            var keywordWasUsed = (new RegExp(keyword, 'i')).test(addon.content);
                            var senderNotIgnored = values.indexOf(addon.sender.id) == -1;

                            if (keywordWasUsed && senderNotIgnored) {
                                bot.data.discoveryIgnore[keyword].push(addon.sender.id);
                                bot.showDiscovery(keyword)
                                bot.cacheDiscovery();
                            }
                        })
                    })

                    bot.fetchLastAlertTime().then(function(){
                        console.log('fetched last alert time')
                        var tenMinutes = (10 * 60 * 1000);
                        var timeSinceLastAlert;
                        if (bot.data.lastAlertTime) { timeSinceLastAlert = moment().diff(bot.data.lastAlertTime) }
                        console.log('time since last alert', timeSinceLastAlert)
                        if (timeSinceLastAlert === undefined || timeSinceLastAlert >= tenMinutes) {
                            console.log('last alert was > 10 minutes ago (or it never happened)');
                            //check to see if there is anything on this room's list
                            bot.fetchData().then(function(){
                                if (bot.data.list === undefined || bot.data.list.length == 0) {
                                    console.log('bot.data.list is undefined of zero-length')
                                    //list is empty so we just update the timer
                                } else {
                                    //spam the room
									bot.fetchWhoToBug().then(function(){
										console.log('room getting spammed right now')
										var emoticon = _.sample(['(yeah)', '(nobullshit)', '(feelsbadman)', '(emg)'])
										bot.respondText(bot.data.whoToBug + ' there are items waiting in QueueBot!  ' +
											'Use "/qbot list" to see them. ' + emoticon, 'red')
									})
                                }
                            });

                            bot.cacheAlertTime();
                        }
                    })

                };
            };
		},

		// THINGS THAT JUST DISPLAY THINGS

		respondText: function (message, color){
			// USE THIS FOR EMOTICONS
			var color = color || 'green'
			addon.roomClient.sendNotification(message, { color: color, notify: true, format: 'text'});

		},

		respondHtml: function (message, color){
			// USE THIS FOR LINKS AND STYLE
			var color = color || 'green'
			addon.roomClient.sendNotification(message, { color: color, notify: true, format: 'html'});

		},

		showHelp: function () {
			var helpText = '/code <<<  QBOT Help  >>>\n' +
				'/qbot help\t\tdisplay this message\n' +
				'/qbot add HC-#####\tadd issue to the queue\n' +
				'/qbot remove [index]\tremove issue from the queue\n' +
				'/qbot list\t\tdisplay the list of all issues\n' +
				'/qbot bug @name1 @name2\tset nag recipients';

			bot.respondText(helpText);
		},

		test: function () {
			// TESTS GO HERE
		},

		likeTurtles: function () {
			//var team = Array('@jervill', '@mention', '@heyyou', '@ChocolateThunder', '@loren', '@DannyNav');
			//var qa = team[Math.floor(Math.random() * team.length)];
			var qa = 'QBot';
			var flairs = Array('(awkwardturtle)', '(anatolinemo)', '(bunny)', '(goat)', '(parrot)', '(koala)');
			var flair = flairs[Math.floor(Math.random() * flairs.length)];

			bot.respondText(qa + ' LIKES TURTLES! ' + flair);
			keen.addEvent(
				"liked_turtles", {
					"user_id": addon.sender.id,
					"room_id": addon.room.id,
					"message": addon.content
				}, function(err, res) {}
			);
		},

		calmDown: function () {
			var thereThere = "┬─┬ノ( ಠ_ ಠノ)";
			bot.respondText("whoa. let's just calm down now " + thereThere)
			keen.addEvent(
				"feng_shui", {
					"user_id": addon.sender.id,
					"room_id": addon.room.id,
					"message": addon.content
				}, function(err, res) {}
			);
		},

		hardReset: function () {
			bot.data = null;
			bot.cacheData();
			bot.respondText('(troll) i deed what you asked me to')
		},


        // discoveryIgnore may be undefined if it's a new keyword (or if migrating from older version of QueueBot)
        massageKeywordData: function(keyword) {
            //FIXME these lines do bad things when bot.data hasn't been fetched
            //if (typeof bot.data.discoveryIgnore === 'undefined') { bot.data.discoveryIgnore = {} }
            //bot.data.discoveryIgnore['demo'] = bot.data.demoIgnore
            //bot.data.discoveryIgnore['kickoff'] = bot.data.kickoffIgnore
            if (keyword && typeof bot.data.discoveryIgnore[keyword] === 'undefined') { bot.data.discoveryIgnore[keyword] = [] }
        },

        showDiscovery: function(keyword) {
            bot.respondHtml('Do you need a <b>' + keyword + '</b>?  Type <b>/qbot add [description]</b> to join the queue!');
            keen.addEvent(
                "discovery", {
                    "keyword": keyword,
                    "user_id": addon.sender.id,
                    "room_id": addon.room.id,
                    "message": addon.content
                }, function (err, res) {
                }
            );
        },

        showIgnore: function () {
			bot.fetchDiscovery().then(function(){
                var msg = '';

                _.each(bot.data.discoveryIgnore, function (ignoreList, key) {
                    if (ignoreList.length == 0) {
                        msg += '(success) Everyone gets ' + key + ' reminders!\n'
                    } else {
                        msg += 'These users will not get ' + key + ' reminders:\n';
                        _.each(ignoreList, function(item, i) {
                            msg += (i + 1) + '. ' + item + '\n';
                        });
                    }
                    msg += '\n'
                });

				bot.respondText(msg);
			});
		},

		showList: function (){
			bot.fetchData().then(function(){
				console.log('done fetching!!!!! SO WE THINK');
				if (bot.data.list === undefined || bot.data.list.length == 0) {
					bot.respondText('(foreveralone) Nothing to see here.')
				} else {
					msg = '<b>Current To-Do List:</b><br>';
					var messageColor = 'green'; //by default, at least

					for (var i = 0; i < bot.data.list.length; i++) {
						item = bot.data.list[i];

						// FOR LEGACY ITEMS, timeSince WILL BE UNDEFINED
						if (item.time === undefined) {
							var timeSince = "";
							console.log("TIME WAS NIL SO I DID A THING " + timeSince);
						} else {

							// MOMENT JS -- AUTOMAGICALLY DETERMINES UNITS
							var timeSince = moment(item.time).fromNow();
						}

						var redAge = 20 * 60 * 1000; //20 minutes
						var yellowAge = 10 * 60 * 1000; //10 minutes
						var itemAge = moment().diff(item.time);

						//add appropriate formatting to the timeSince string, based on item age

						timeSince = '<i>' + timeSince + '</i>'; //always italicize the time

						if (itemAge >= redAge) {				//if red, apply red formatting
							timeSince += ' (!)';
							messageColor = 'red';
						}

						if (itemAge >= yellowAge) {				//if yellow or red, apply yellow formatting too
							timeSince = '<b>' + timeSince + '</b>';
							if (messageColor != 'red') { messageColor = 'yellow' }; //but don't set it to yellow if we've already seen a red
						}

						msg += '<b>' + (i + 1) + '</b>. ' + item.key + ' for ' + item.name + ', ' + timeSince + '<br>';
					}
					bot.respondHtml(msg, messageColor);
				}
                bot.cacheAlertTime(moment().format()); //make showList reset the timer
			});
		},


		// THINGS THAT ACTUALLY DO THINGS

		bug: function () {
			var whoWeShouldBug = addon.content.replace('/qbot bug ', '').replace(/\s+$/,'');
			bot.cacheWhoToBug(whoWeShouldBug)
			bot.respondText('I shall enjoy bugging them (noprobalo)')
		},

		add: function (issueText){
			var issueText = issueText || addon.content.replace('/qbot ', '').replace(/^add\s/i, '');

			bot.fetchData().then(function () {
				var timeAdded = moment().format();
				var itemId = uuid.v1();
				var jiraBase = "https://jira.atlassian.com/browse/";
				var jiraRegex = new RegExp("HC-\\d+", "igm");
				var positionRegex = /\sat\s(\d+)$/;

				var atPosition = bot.data.list.length;

				if (positionRegex.test(issueText)) {
					atPosition = parseInt(issueText.replace(/^.*at\s/, "")) - 1;
					issueText = issueText.replace(positionRegex, "")
				}

				if (jiraRegex.test(issueText)) {
					// DETECT JIRA ISSUE FORMAT TEXT (HC-####) AND REPLACE THIS STRING WITH HTML (jiraBase + HC-KEY)
					// Changed to use MATCH for multiple matches, jiraIDs is now an array
					var jiraIDs = issueText.match(jiraRegex)

					// iterate over the collection, replace all with HTML link tags
					_.each(jiraIDs, function(id){
						issueText = issueText.replace(id, '<a href="' + jiraBase + id + '"><b>' + id + '</b></a>')
					})
				}

				var listItem = {
                    id: itemId,
                    key: issueText,
					name: addon.sender.name,
					userID: addon.sender.id,
					time: timeAdded
				};

                keen.addEvent(
                    "item_added", {
                        "item_id": listItem.id,
                        "user_id": addon.sender.id,
                        "room_id": addon.room.id
                    }, function(err, res) {}
                );

				console.log("Adding:", issueText);
				bot.data.list.splice(atPosition, 0, listItem);
				bot.cacheData();
				bot.respondHtml(addon.sender.name + ' added ' + issueText + ' in position ' + (bot.data.list.indexOf(listItem) + 1) + ' of ' + (bot.data.list.length) + '!');
			})
		},

		addKickoff: function() { //convenience wrapper for add [foo] kickoff
			var issueText = addon.content.replace('/qbot kickoff', '') + ' kickoff';
			bot.add(issueText);
		},

		addDemo: function() { //convenience wrapper for add [foo] demo
			var issueText = addon.content.replace('/qbot demo', '') + ' demo';
			bot.add(issueText);
		},

        createKeyword: function() {
            var keyword = addon.content.replace('/qbot create keyword ', '').replace(/\s.*$/, '');
            bot.fetchDiscovery().then(function() {
                console.log(keyword)
                if (typeof bot.data.discoveryIgnore[keyword] === 'undefined') {
                    bot.respondText('Gonna create "' + keyword + '" when Anthony fixes this (yey)\n')
                    //bot.data.discoveryIgnore[keyword] = []
                    //bot.respondText('Created "' + keyword + '" keyword!')
                    //bot.cacheDiscovery();
                } else {
                    bot.respondText('Unable to create that keyword (sadplanet)')
                }
            })
        },

        listKeywords: function() {
            bot.fetchDiscovery().then(function() {
                msg = 'I listen for:<br>'
                _.each(bot.data.discoveryIgnore, function(list, keyword, index) {
                    console.log(keyword)
                    msg += '<i>' + keyword + '</i><br/>'
                })
                bot.respondHtml(msg)
            })
        },

		// THIS THING DELETES THE ENTIRE TO-DO LIST THING
		destroyList: function() {
			bot.fetchData().then(function() {
				console.log('Destroying list!');
				bot.respondText('R U sure? (lol) j/k I trust you, (allthethings) have been deleted.');
				bot.data.list = [];
				bot.cacheData();
			})
		},


		// THIS THING (HIDDEN) DELETES THE IGNORE LIST THINGY
		destroyIgnore: function() {
			console.log('Destroying list!');
			bot.respondText('R U sure? (lol) j/k I trust you, (allthethings) have been deleted.');
			bot.data.discoveryIgnore = {
				kickoff: [],
				demo: [],
			};
			bot.data.demoIgnore = [];
			bot.data.kickoffIgnore = [];
			bot.cacheDiscovery();
		},

		// THIS THING REMOVES A TO DO WHEN ITS DONE
		dismissItem: function() {
			bot.fetchData().then(function() {
				// lets user specify multiple item numbers per request
				var indices = addon.content.match(/\d+/gm);

				// HACK: remove items in descending order so indexes don't change while removing
				_.each(indices.sort().reverse(), function(index) {
					if (index === null) {
						bot.respondText("Try dismissing by item number. (playasateam)");
						return;
					}
					if (bot.data.list.length == 0) {
						bot.respondText("There are no items in the list (awyeah)");
						return;
					} else if (index < 1 || index > bot.data.list.length) {
						bot.respondText("Nope. There are " + bot.data.list.length + " items in the list.");
						return;
					}
					var listItem = bot.data.list[index - 1];
					var removedKey = listItem.key;
					var itemAge = moment().diff(moment(listItem.time))

					keen.addEvent(
						"item_dismissed", {
							"item_id": listItem.id,
							"item_age_millis": itemAge,
							"item_age_minutes": itemAge / 1000 / 60.0,
							"dismisser": addon.sender.id,
							"room_id": addon.room.id,
						}, function (err, res) {
						}
					);

					if (index > -1) {
						bot.data.list.splice(index - 1, 1);
					}
					bot.cacheData();
					bot.respondHtml('<b>' + removedKey + '</b> has been removed. There ' + (bot.data.list.length == 1 ? 'is 1 issue left.' : 'are ' + bot.data.list.length + ' issues left.'));
				})
			});
		},

		//  THIS IS A THING ANTHONY MADE AND ITS IMPORTANT BUT I DONT UNDERSTAND IT
		cacheData: function() {
			console.log('<<<storing data in redis');
			addon.tenantStore.set(REDIS_KEY, bot.data);
		},

		// THIS IS ANOTHER THING ANTHONY MADE ASK HIM
		fetchData: function() {
			console.log('>>>fetching data from redis');
			return addon.tenantStore.get(REDIS_KEY).then(function(data){
				//console.log('Fetched this:\n', data);
				if (data === null || Object.keys(data).length === 0) {

				} else {
					bot.data = data;
				}
			});
		},

        cacheDiscovery: function() {
            addon.tenantStore.set(REDIS_KEY + '_discovery', bot.data.discoveryIgnore);
        },

        fetchDiscovery: function() {
            return addon.tenantStore.get(REDIS_KEY + '_discovery').then(function(data){
                //console.log('Fetched discovery data:\n', data);
                if (data === null || Object.keys(data).length === 0) {

                } else {
                    bot.data.discoveryIgnore = data;
                }
            });
        },

        cacheAlertTime: function(timestamp) {
            timestamp = timestamp || moment().format()
            addon.tenantStore.set(REDIS_KEY + '_alert_time', timestamp);
        },

        fetchLastAlertTime: function() {
            return addon.tenantStore.get(REDIS_KEY + '_alert_time').then(function(data){
                console.log('fetched lastAlertTime:', data)
                if (data === null) {
                    console.log('no alert time data?')
                } else {
                    bot.data.lastAlertTime = data;
                }
            });
        },

		cacheWhoToBug: function(whoToBug) {
			whoToBug = whoToBug || bot.data.whoToBug
			addon.tenantStore.set(REDIS_KEY + '_who_to_bug', whoToBug);
		},

		fetchWhoToBug: function() {
			return addon.tenantStore.get(REDIS_KEY + '_who_to_bug').then(function(data){
				if (data === null) {
					bot.data.whoToBug = '@here'
				} else {
					bot.data.whoToBug = data;
				}
			});
		},

        keenShowAverageTime: function() {
            //var outputMessage = 'QueueBot statistics (since July 15, 2015)\n\n';

            var path = "/queries/average",
                params = {
                    "event_collection": "item_dismissed",
                    "target_property": "item_age_minutes",
                    "filters": [{
                        "property_name": "room_id",
                        "operator": "eq",
                        "property_value": addon.room.id
                    }]
            };

            keen.request('get', 'read', path, params, function(err, responseBody) {
                if (err) {
                    console.log('error:', err)
                } else {
                    console.log(responseBody)
                }
                var averageAge = +parseFloat(responseBody.result).toFixed(1)
                var outputMessage = 'Average time an item spends on your list: ' + averageAge + ' minutes (wellmemed)'
                bot.respondText(outputMessage)
            })

        },


	}

	// RETURNS THE BOT FOR A FULL REFUND WITH RECEIPT
	return bot;
};

// IMPORTS AND EXPORTS

module.exports = QueueBot;